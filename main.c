#include <stdio.h>
#include <stdlib.h>
#include <time.h>                                //Usado somente para inicializar o gerador random

#define Max 100                                  //Define o n�mero de elementos

//-----------------------------------------------
typedef struct No *Ptr_No;                       //Define o Apontador
//-----------------------------------------------
struct No{                                       //Define a Estrutura de cada n�
	int Valor;
	Ptr_No Esquerdo;                                       //Apontadores usados para ligar aos n�s filhos
	Ptr_No Direito;
};
//-----------------------------------------------
int InserirNo (Ptr_No *No,int Valor){           //Procedimento para inser��o de um novo n�
	int c = 0;                                             //Variavel para controle se o valor j� existe na �rvore
	if(*No==NULL){                                         //Verifica se existe o n� ja existe quando NULL n�o existe
		*No=(struct No*)malloc(sizeof(struct No));         //Reserva mem�ria do tamanho do n� e retorna o endere�o
		(*No)->Esquerdo=NULL;                              //Inicializa os apontadores com NULL
		(*No)->Direito=NULL;
		(*No)->Valor=Valor;
		c = 0;
	}else{                                                 //Se existir o n�
		if(Valor<(*No)->Valor){                            //Percorre a esquerda se Valor � menor que o valor do n� actual
			return c + InserirNo(&(*No)->Esquerdo,Valor);  //Tenta inserir Valor (Fun��o recursiva)		
		}
		
		if(Valor>(*No)->Valor) {                            //Percorre a direita se Valor � maior que o valor do n� actual
			return c + InserirNo(&(*No)->Direito,Valor);    //Tenta inserir Valor (Fun��o recursiva)	
		}
		
		if(Valor==(*No)->Valor){
			return 1;	
		}
		
	return c;	
	}
}
//-----------------------------------------------
void ExibirInOrder (Ptr_No No){                 //Imprime todos os valores a percorrer a �rvore em In Order
	if (No != NULL){
		ExibirInOrder(No->Esquerdo);                       //Percorrer sempre a esquerda
		printf("%d ",No->Valor);                           //Imprimir o Valor
		ExibirInOrder(No->Direito);                        //Percorrer a direita
	}
}
//-----------------------------------------------
void ExibirPosOrder (Ptr_No No){                 //Imprime todos os valores a percorrer a �rvore em Post Order
	if (No != NULL){
		ExibirInOrder(No->Direito);                        //Percorrer sempre a ireita
		printf("%d ",No->Valor);                           //Imprimir o Valor
		ExibirInOrder(No->Esquerdo);                       //Percorrer a esquerda
	}
}
//-----------------------------------------------
void ExibirPreOrder (Ptr_No No){                 //Imprime todos os valores a percorrer a �rvore em Pre Order
	if (No != NULL){
		printf("%d ",No->Valor);                           //Imprimir o Valor
		ExibirPreOrder(No->Esquerdo);                       //Percorrer sempre a esquerda
		ExibirPreOrder(No->Direito);                        //Percorrer a direita
	}
}
//-----------------------------------------------
int ProcuraValor(Ptr_No No, int Valor,int Contador){ //Procura o Valor dado e retorna  n� de intera��es
	if (No == NULL) {
		Contador++;
		printf ("Procurado %d, Nao Encontrado, Interacoes %d\n",Valor,Contador);
	} else {
		if (Valor == No->Valor){
			Contador++;
			printf ("Procurado %d, Encontrado %d, Interacoes %d\n",Valor,No->Valor,Contador);
		};
		if (Valor < No->Valor){
			Contador++;
			printf ("Procurado %d, Encontrado %d, Interacoes %d ESQUERDA\n",Valor,No->Valor,Contador);
			Contador=ProcuraValor(No->Esquerdo,Valor,Contador);
		};
		if (Valor > No->Valor){
			Contador++;
			printf ("Procurado %d, Encontrado %d, Interacoes %d DIREITA\n",Valor,No->Valor,Contador);
			Contador=ProcuraValor(No->Direito,Valor,Contador);
		};
	};
	return Contador;
}
//-----------------------------------------------
int DaNos(Ptr_No No){                            //Conta o n�mero de n�s em uma �rvore
	if(No==NULL) return 0;
	else return 1 + DaNos(No->Esquerdo) +DaNos(No->Direito);
}
//-----------------------------------------------
int DaFolhas(Ptr_No No){                         //Conta o n�mero de folhas de um �rvore
	if(No==NULL) return 0;
	if (No->Esquerdo==NULL && No->Direito==NULL) return 1; //Folhas: N�o tem n�s ligados nem a direita nem a esquerda
	return 0 + DaFolhas(No->Esquerdo) + DaFolhas(No->Direito) ;
}
//-----------------------------------------------
int DaMaior(int V1, int V2){                     //Fun��o Auxiliar para retornar o maior Valor
	if(V1>V2) return V1;
	else return V2;
}
//-----------------------------------------------
int DaAltura(Ptr_No No){                        //Retorna a altura da �rvore (Maior distancia entre um n� e a raiz)
	if((No==NULL) || (No->Esquerdo==NULL && No->Direito==NULL) )return 0;
	else return 1 + DaMaior(DaAltura(No->Esquerdo),DaAltura(No->Esquerdo));
}
//-----------------------------------------------
int main(int argc, char *argv[]) {
	
	Ptr_No ROOT = NULL;                                    //Define o ponteiro para p ROOT da BST como NULL para indicar que a �rvore esta vazia 
	int i = NULL, Valor=NULL;	
	int num = NULL;
	int temp = NULL;
	
	srand ( time(NULL) );                                  //Inicializa o gerador random
	printf ("A gerar numeros aleatorios:\n");
	i=0;
	while (i<Max){
		num = rand()%Max;                                  //Gera valor aleat�rio e armazena em num	
		temp = InserirNo(&ROOT,num);
		if (temp!=1){                                      //Verifica se o numero j� foi inserido
			printf("%d ",num);
			i++;
		};	
	};
	printf ("\n");
	printf ("--------------------------------------------------------------------------------");
	
	printf("In Order:\n");
	ExibirInOrder(ROOT);
	printf("\n");
	printf ("--------------------------------------------------------------------------------");
	
	printf("Pos Order:\n");
	ExibirPosOrder(ROOT);
	printf("\n");
	printf ("--------------------------------------------------------------------------------");	
	
	printf("Pre Order:\n");
	ExibirPreOrder(ROOT);
	printf("\n");
	printf ("--------------------------------------------------------------------------------");	
	
	printf("Valor a procurar: "); scanf("%d",&Valor);
	printf("Interacoes: %d",ProcuraValor(ROOT,Valor,0));
	printf("\n");
	printf ("--------------------------------------------------------------------------------");	
	
	printf("Numero de nos: %d\n",DaNos(ROOT));
	printf("Numero de folhas: %d\n",DaFolhas(ROOT));
	printf("Altura da arvore: %d\n",DaAltura(ROOT));

	return 0;
}

